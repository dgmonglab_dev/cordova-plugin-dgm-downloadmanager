package com.indigo.android.plugin.downloader;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import java.io.File;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import com.indigo.android.plugin.downloader.FileProvider;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.HashMap;
import java.util.Objects;

import capacitor.android.plugins.BuildConfig;

public class Downloader extends CordovaPlugin {

    private static final String ACTION_DOWNLOAD = "download";
    public boolean open = false;
    private Activity cordovaActivity;
    private DownloadManager downloadManager;
    private HashMap<Long, Download> downloadMap;

    @Override
    protected void pluginInitialize()
    {
        cordovaActivity = this.cordova.getActivity();

        downloadManager = (DownloadManager) cordovaActivity.getSystemService(Context.DOWNLOAD_SERVICE);
        downloadMap = new HashMap();

        cordovaActivity.registerReceiver(downloadReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        if (ACTION_DOWNLOAD.equals(action)) {
            return download(args, callbackContext);
        }
        return false;
    }

    private boolean download(JSONArray args, CallbackContext callbackContext)
    {

        try {
            // Json object 로 값 가져오기
            JSONObject arg_object = args.getJSONObject(0);
            String fileName = arg_object.getString("url").substring(arg_object.getString("url").lastIndexOf("/") + 1, arg_object.getString("url").length());
            String title = "다운로드 매니저";
            String folder = Environment.DIRECTORY_DOWNLOADS;
            String description = fileName + " 파일 다운로드";
            open = Boolean.valueOf(arg_object.getString("open"));

            Uri uri = Uri.parse(arg_object.getString("url"));
            // 다운로드 객체 생성
            Download mDownload = new Download(fileName, folder, callbackContext);

            DownloadManager.Request request = new DownloadManager.Request(uri);

            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE); // 와이파이, 데이터 두가지 경우 다운로드 가능

            request.setAllowedOverRoaming(true); // 데이터 로밍 시 파일 다운로드 가능

            request.setTitle(fileName);

            request.setDescription(description);

            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

            request.setDestinationInExternalPublicDir(folder, fileName);
            downloadMap.put(downloadManager.enqueue(request), mDownload); // 다운로드 객체 내용 저장

            return true;

        } catch (Exception e) {

            System.err.println("Exception: " + e.getMessage());
            callbackContext.error(e.getMessage());

            return false;
        }
    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            DownloadManager.Query query = new DownloadManager.Query();
            Long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
            query.setFilterById(downloadId);
            Cursor cursor = downloadManager.query(query);
            String mime = downloadManager.getMimeTypeForDownloadedFile(downloadId);
            if (cursor.moveToFirst()){

                Download currentDownload = downloadMap.get(downloadId);
                downloadMap.remove(currentDownload);

                int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                int status = cursor.getInt(columnIndex);
                int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
                int reason = cursor.getInt(columnReason);

                String fileUri = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                File mFile = new File(Objects.requireNonNull(Uri.parse(fileUri).getPath()));
                Uri fileOpener = FileProvider.getUriForFile(cordovaActivity, cordovaActivity.getPackageName() + ".provider", mFile);
                switch (status) {
                    case DownloadManager.STATUS_SUCCESSFUL:
                        try {
                            JSONObject entry = new JSONObject();
                            entry.put("result", true);
                            if(open){
                                openFile(fileOpener, mime);
                            }
                            currentDownload.callbackContext.success(entry);
                        } catch (Exception e) {
                            System.err.println("Exception: " + e.getMessage());
                            currentDownload.callbackContext.error(e.getMessage());
                        }
                        break;
                    case DownloadManager.STATUS_FAILED:
                        currentDownload.callbackContext.error(reason);
                        break;
                    case DownloadManager.STATUS_PAUSED:
                    case DownloadManager.STATUS_PENDING:
                    case DownloadManager.STATUS_RUNNING:
                    default:
                        break;
                }
            }
        }

    };

    private class Download {
        public String path;
        public String folder;
        public CallbackContext callbackContext;

        Download(String path, String folder, CallbackContext callbackContext) {
            this.path = path;
            this.folder = folder;
            this.callbackContext = callbackContext;
        }
    }
    private void openFile(Uri file, String mime) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setDataAndType(file, mime);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        i.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        cordovaActivity.startActivity(i);
    }

}
